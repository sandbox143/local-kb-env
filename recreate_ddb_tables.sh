#!/usr/bin/env sh

echo "removing tables ..."

## SUBSCRIPTION API
awslocal dynamodb delete-table --table-name "subscription-api-idempotency-table2" --region eu-west-2
awslocal dynamodb create-table --cli-input-json "file:///Users/joaosousa/Bango/Work/local-kb-env/localstack/data/createidempotency.json" --region eu-west-2

awslocal dynamodb delete-table --table-name "subscription-api-kill-bill-external-mapping" --region eu-west-2
awslocal dynamodb create-table --cli-input-json "file:///Users/joaosousa/Bango/Work/local-kb-env/localstack/data/createmappingtable.json" --region eu-west-2

awslocal dynamodb delete-table --table-name "ddb-ew2-d-subscription-api-id-mapping" --region eu-west-2
awslocal dynamodb create-table --cli-input-json "file:///Users/joaosousa/Bango/Work/local-kb-env/localstack/data/idmappingtable.json" --region eu-west-2


## SUBSCRIPTION CANCEL RESUBSCRIPTION
awslocal dynamodb delete-table --table-name "subscription_cancel_and_recreate_events" --region eu-west-2
awslocal dynamodb create-table --cli-input-json "file:///Users/joaosousa/Bango/Work/local-kb-env/localstack/data/subscription_cancel_and_recreate_events.json" --region eu-west-2

awslocal dynamodb delete-table --table-name "scheduled_updates" --region eu-west-2
awslocal dynamodb create-table --cli-input-json "file:///Users/joaosousa/Bango/Work/local-kb-env/localstack/data/scheduled_updates.json" --region eu-west-2


## NOTIFICATION API
awslocal dynamodb delete-table --table-name "notifications_details" --region eu-west-2
awslocal dynamodb create-table --cli-input-json "file:///Users/joaosousa/Bango/Work/local-kb-env/localstack/data/notifications_details.json" --region eu-west-2

awslocal dynamodb delete-table --table-name "retry_message_config" --region eu-west-2
awslocal dynamodb create-table --cli-input-json "file:///Users/joaosousa/Bango/Work/local-kb-env/localstack/data/retry_message_config.json" --region eu-west-2

awslocal dynamodb delete-table --table-name "message_retry_schedule" --region eu-west-2
awslocal dynamodb create-table --cli-input-json "file:///Users/joaosousa/Bango/Work/local-kb-env/localstack/data/message_retry_schedule.json" --region eu-west-2

## EVENTS ROUTER
awslocal dynamodb delete-table --table-name "ddb-d-ew2-http-ready-events-for-retry" --region eu-west-2
awslocal dynamodb create-table --cli-input-json "file:///Users/joaosousa/Bango/Work/local-kb-env/localstack/data/retry_schechule_web_processor.json" --region eu-west-2

## PRICE CHANGE NOTIFICATIONS
awslocal dynamodb delete-table --table-name "ddb-localstack-subscription-plan-mapping" --region eu-west-2
awslocal dynamodb create-table --cli-input-json "file:///Users/joaosousa/Bango/Work/local-kb-env/localstack/data/create-ddb-subscription-plan-mapping.json" --region eu-west-2

awslocal dynamodb delete-table --table-name "ddb-localstack-subscriptions-price-changes" --region eu-west-2
awslocal dynamodb create-table --cli-input-json "file:///Users/joaosousa/Bango/Work/local-kb-env/localstack/data/create-ddb-subscriptions-price-changes.json" --region eu-west-2

echo "Remove tables done"

