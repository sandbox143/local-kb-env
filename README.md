# local-kb-env

Killbill with plugins and api modules

## Getting started

1. create or get gitlab access token 
2. edit `.env`, put values into `CI_GITLAB_MODULES_REG_USERNAME` & `CI_GITLAB_MODULES_REG_TOKEN`
3. ensure your local git client has authentication for bango gitlab projects
- `clone-and-run.sh` starts the environment, usually can be re-run after a failure
- `stop-and-remove.sh` tears down the environment (but not images)

### Postman test suite
- location in "postman_collections/*.json"
- load "*BT Adaptor - Automation.postman_collection*" suite
- load "*AWS Subscriptions - Test - Automation.postman_environment*" environment config, mark it active
- run a test from "Add Product/Successful" scenarios. Most "Add" scenarios work. Other scenarios may require dedicated configuration or data provisioning.

### Details of "subscription-cancel-and-resubscribe" AWS setup
- dynamodb is local via Localstack. For remote AWS, comment out `DYNAMO_ENDPOINT_URL`, then actual AWS instance will be selected by AWS keys
- the application doesn't provide a means for custom AWS endpoint URL for secrets, this is stored in pawel.stobinski@bango.com AWS space

## Authors and acknowledgment
base from: Alexei Melnic 
Alexei@bango.com

## Core KillBill

1. execute: docker-compose -f docker-compose-core.yaml up -d