-- USE events;

create table eventsapi
(
    eventId            varchar(36) not null,
    destinationUrl     varchar(256) DEFAULT '',
    scheduledTime      datetime    not null,
    eventType          text        not null,
    eventMetadata      text        not null,
    event              text        not null,
    groupId            text        not null,
    originatingHost    text        not null,
    originatingService text        not null,
    editTimeStamp      datetime    not null,
    retryAttempt       int     DEFAULT 0,
    locked             boolean DEFAULT false,
    constraint eventsapi_pk
        primary key (eventId, destinationUrl)
);

CREATE TABLE failedEvents
(
    failedEventId      serial auto_increment not null,
    eventId            varchar(36)           not null,
    destinationUrl     varchar(256),
    failureReasonCode  text                  not null,
    failureReason      text                  not null,
    failureSource      text                  not null,
    dateInserted       datetime              NOT NULL,
    scheduledTime      datetime              NOT NULL,
    eventType          text                  not null,
    eventMetadata      text                  not null,
    event              text                  not null,
    groupId            text                  not null,
    retryAttempt       int DEFAULT 0,
    originatingHost    text                  not null,
    originatingService text                  not null,
    constraint failedevent_pk
        primary key (failedEventId)
);

CREATE TABLE subscriptions_events_mapping
(
    subscription_id    varchar(36) not null,
    eventId            varchar(36) not null unique,
    constraint subscriptions_events_mapping_pk primary key (eventId, subscription_id),
    foreign key (eventId) references eventsapi(eventId) ON DELETE CASCADE
);

CREATE INDEX subscriptions_events_mapping_index ON subscriptions_events_mapping (subscription_id);