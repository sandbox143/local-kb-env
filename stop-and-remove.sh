#!/bin/sh
set -e
bold=$(tput bold)
normal=$(tput sgr0)

echo ${bold}removing containers and volumes...${normal}
docker-compose down --volumes

## REMOVE ALL images before new build
#echo "${bold}"removing all images..."${normal}"
#docker system prune -a

## REMOVE local-kb-env images before new build
SUBS_IMAGES=$(docker images | grep "local-kb-env" | awk '{ print $3 }');
echo $SUBS_IMAGES
for image in $SUBS_IMAGES; do
 docker rmi -f "$image"
done

echo ${bold}done${normal}