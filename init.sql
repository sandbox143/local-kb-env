USE killbill;


DROP TABLE IF EXISTS bango_responses;
CREATE TABLE bango_responses
(
    record_id                 serial,
    kb_account_id             char(36)     NOT NULL,
    kb_payment_id             char(36)     NOT NULL,
    kb_payment_transaction_id char(36)     NOT NULL,
    transaction_type          varchar(32)  NOT NULL,
    transaction_id            varchar(255),
    amount                    numeric(15, 9),
    currency                  char(3),
    bango_id                  varchar(255) NOT NULL,
    additional_data           longtext DEFAULT NULL,
    created_date              datetime     NOT NULL,
    kb_tenant_id              char(36)     NOT NULL,
    PRIMARY key (record_id)
);

CREATE INDEX bango_responses_kb_payment_id ON bango_responses(kb_payment_id);
CREATE INDEX bango_responses_kb_payment_transaction_id ON bango_responses(kb_payment_transaction_id);
CREATE INDEX bango_responses_bango_id ON bango_responses(bango_id);


DROP TABLE IF EXISTS bango_payment_methods;
CREATE TABLE bango_payment_methods
(
    record_id            serial,
    kb_account_id        char(36)     NOT NULL,
    kb_payment_method_id char(36)     NOT NULL,
    bango_id             varchar(255) NOT NULL,
    is_deleted           smallint     NOT NULL DEFAULT 0,
    additional_data      longtext              DEFAULT NULL,
    created_date         datetime     NOT NULL,
    updated_date         datetime     NOT NULL,
    kb_tenant_id         char(36)     NOT NULL,
    PRIMARY key (record_id)
);
CREATE UNIQUE INDEX bango_payment_methods_kb_payment_id ON bango_payment_methods(kb_payment_method_id);
CREATE INDEX bango_payment_methods_bango_id ON bango_payment_methods(bango_id);




ALTER TABLE bango_responses ADD bango_request_id bigint(20) null;
drop table if exists bango_requests;
create table bango_requests
(
    record_id                 serial,
    kb_account_id             char(36)     not null,
    kb_payment_id             char(36)     not null,
    kb_payment_transaction_id char(36)     not null,
    transaction_type          varchar(32)  not null,
    bango_id                  varchar(255) not null,
    extension_data            longtext default null,
    payment_items             longtext default null,
    external_transaction_key  varchar(36) null,
    external_transaction_id   varchar(36) null,
    transaction_id            varchar(36) null,
    invoice_id                varchar(36) null,
    created_date              datetime     not null,
    kb_tenant_id              char(36)     not null,
    bango_response_id         bigint(20) null,
    primary key (record_id)
);

create index bango_requests_kb_payment_id on bango_requests(kb_payment_id);
create index bango_requests_kb_payment_transaction_id on bango_requests(kb_payment_transaction_id);
create index bango_requests_bango_id on bango_requests(bango_id);




ALTER TABLE bango_requests MODIFY COLUMN external_transaction_id varchar(40) null;
ALTER TABLE bango_requests MODIFY COLUMN external_transaction_key varchar(40) null;




drop table if exists catalog_product;
create table catalog_product
(
    record_id    serial,
    name         char(50) not null,
    pretty_name  char(50),
    category     char(50)          DEFAULT 'BASE',
    kb_tenant_id char(50) not null,
    created_date datetime not null DEFAULT CURRENT_TIMESTAMP,
    updated_date datetime not null DEFAULT CURRENT_TIMESTAMP,
    primary key (record_id)
);

create index catalog_product_kb_payment_id on catalog_product(kb_tenant_id);
create unique index catalog_product_name_tenant on catalog_product(kb_tenant_id, name);

drop table if exists catalog_plan;
create table catalog_plan
(
    record_id         serial,
    plan_id           char(50) not null,
    plan_name         char(50) not null,
    is_active         smallint not null default 0,
    product_record_id BIGINT UNSIGNED,
    kb_tenant_id      char(50) not null,
    created_date      datetime not null DEFAULT CURRENT_TIMESTAMP,
    updated_date      datetime not null DEFAULT CURRENT_TIMESTAMP,
    primary key (record_id),
    FOREIGN KEY (product_record_id)
        REFERENCES catalog_product (record_id)
        ON DELETE CASCADE
);

create index catalog_plan_kb_tenant_id on catalog_plan(kb_tenant_id);
create unique index catalog_plan_plan_name on catalog_plan(plan_id);
create unique index catalog_plan_name_tenant on catalog_plan(kb_tenant_id, plan_name);

drop table if exists catalog_plan_phase;
create table catalog_plan_phase
(
    record_id           serial,
    sequence            smallint     not null,
    billing_description varchar(255) not null,
    type                char(20)     not null default 'EVERGREEN',
    billingPeriod       char(20)     not null default 'MONTHLY',
    currency            char(3)      not null default 'GBP',
    phase_unit          char(30)     not null default 'UNLIMITED',
    phase_length        smallint,
    phase_duration      char(10)     not null,
    price_amount        float,
    price_currency      char(3)      not null default 'GBP',
    plan_record_id      BIGINT UNSIGNED,
    created_date        datetime     not null DEFAULT CURRENT_TIMESTAMP,
    updated_date        datetime     not null DEFAULT CURRENT_TIMESTAMP,
    primary key (record_id),
    FOREIGN KEY (plan_record_id)
        REFERENCES catalog_plan (record_id)
        ON DELETE CASCADE
);




ALTER TABLE catalog_product MODIFY COLUMN name char(255) not null;
ALTER TABLE catalog_product MODIFY COLUMN pretty_name char(255) null;




CREATE INDEX `idx_bango_requests_kb_account_id` ON `bango_requests` (kb_account_id) COMMENT '' ALGORITHM DEFAULT LOCK DEFAULT;




ALTER TABLE catalog_plan_phase ADD COLUMN fixed_price_amount float;
ALTER TABLE catalog_plan_phase ADD COLUMN fixed_price_currency char(3);

drop table if exists bango_subscription_correlation;
create table bango_subscription_correlation
(
    subscription_id VARCHAR(80) NOT NULL,
    correlation_type VARCHAR(80) NOT NULL,
    correlation_value VARCHAR(80) NOT NULL,
    primary key(subscription_id, correlation_type)
);



ALTER TABLE catalog_plan_phase ADD COLUMN period_renewal VARCHAR(255);
ALTER TABLE catalog_plan_phase ADD COLUMN phase_renewal VARCHAR(255);