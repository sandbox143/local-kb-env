#!/usr/bin/env sh

echo ${bold}"Local stack put items STARTING"${normal}

payment_api_url=http://host.docker.internal
#payment_api_url=http://localhost
#payment_api_url=http://payment_api

tenantId=$(curl -v -u admin:password -H "Accept: application/json" "http://127.0.0.1:8080/1.0/kb/tenants?apiKey=bob" | jq '.tenantId' | tr -d '"')

if [[ -z "$tenantId" ]] || [[ "$tenantId" == "null" ]];
then
  echo ${bold}"Local stack couldn't retrieve tenantId"${normal};
  exit;
fi

echo ${bold}"Putting items into tables ..."${normal};

aws --endpoint-url=http://localhost:4566 dynamodb delete-item --table-name notifications_details \
    --key '{"routingKey":{"S":"'$tenantId'//SUBSCRIPTION/INVOICE_ZERO_CHARGE"}}'

aws --endpoint-url=http://localhost:4566 dynamodb put-item \
--table-name notifications_details  \
--item \
    '{
       "routingKey": {
         "S": "KILL_BILL/'$tenantId'//SUBSCRIPTION/INVOICE_ZERO_CHARGE"
       },
       "details": {
         "L": [
           {
             "M": {
               "url": {
                 "S": "'${payment_api_url}':8093/payment/zerocharge/"
               },
               "httpMethod": {
                 "S": "POST"
               },
               "maxAttempts": {
                 "S": "6"
               },
               "username": {
                 "S": "test"
               },
               "password": {
                 "S": "test"
               }
             }
           }
         ]
       }
     }'

aws --endpoint-url=http://localhost:4566 dynamodb delete-item --table-name notifications_details \
    --key '{"routingKey":{"S":"'$tenantId'//SUBSCRIPTION/INITIAL_FEE_REQUEST"}}'

aws --endpoint-url=http://localhost:4566 dynamodb put-item \
--table-name notifications_details  \
--item \
    '{
       "routingKey": {
         "S": "KILL_BILL/'$tenantId'//SUBSCRIPTION/INITIAL_FEE_REQUEST"
       },
       "details": {
         "L": [
           {
             "M": {
               "url": {
                 "S": "'${payment_api_url}':8093/payment/initialfee/"
               },
               "httpMethod": {
                 "S": "POST"
               },
               "maxAttempts": {
                 "S": "6"
               },
               "username": {
                 "S": "test"
               },
               "password": {
                 "S": "test"
               }
             }
           }
         ]
       }
     }'

echo ${bold}"Local stack put items is not DONE"${normal}