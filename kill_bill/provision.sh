#!/bin/sh
set -e
bold=$(tput bold)
normal=$(tput sgr0)

echo ${bold}waiting for killbill api to upload catalogue...${normal}
until curl --location --request GET 'http://localhost:8080/1.0/kb/catalog' --header 'Accept: application/json' --header 'X-Killbill-ApiKey: bob' --header 'X-Killbill-ApiSecret: lazar' --header 'Authorization: Basic YWRtaW46cGFzc3dvcmQ=' >/dev/null
do
    sleep 1
done

echo ${bold}killbill api up, creating tenant...${normal}
curl -v \
    -X POST \
    -u admin:password \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --header "X-Killbill-CreatedBy: demo" \
    --header "X-Killbill-Reason: demo" \
    --header "X-Killbill-Comment: demo" \
    -d '{ "apiKey": "bob", "apiSecret": "lazar"}' \
    "http://127.0.0.1:8080/1.0/kb/tenants"

echo ${bold}verifying tenant was created...${normal}
response=$(curl --write-out '%{http_code}' --silent --output /dev/null -u admin:password -H "Accept: application/json" "http://127.0.0.1:8080/1.0/kb/tenants?apiKey=bob")
if ! [[ 200 =~ $response ]] ; then
   echo "expected tenant was not created" >&2; exit 1
else
   echo "expected tenant was created"
fi

echo ${bold}uploading catalogue...${normal}
curl -v --location 'http://localhost:8080/1.0/kb/catalog' -H "Content-Type: text/xml" -H "X-Killbill-CreatedBy: demo" -H 'X-Killbill-ApiKey: bob' -H 'X-Killbill-ApiSecret: lazar' -H 'Authorization: Basic YWRtaW46cGFzc3dvcmQ=' --data-binary "@kill_bill/config/catalogue.xml"

echo ${bold}verifying catalogue is present in kb...${normal}
text=$(curl --location --request GET 'http://localhost:8080/1.0/kb/catalog' --header 'Accept: application/json' --header 'X-Killbill-ApiKey: bob' --header 'X-Killbill-ApiSecret: lazar' --header 'Authorization: Basic YWRtaW46cGFzc3dvcmQ=')
string="BRITBOX"
if [[ $text == *"$string"* ]]; then
    echo "catalogue OK"
else
    echo "catalogue verification failed"
	exit 1
fi

echo ${bold}provisioning done${normal}
