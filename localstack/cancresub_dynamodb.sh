cancelresub_container_repo_path=/tmp/subscription-cancel-and-resubscribe

echo creating tables for cancel and resubscribe...
awslocal dynamodb create-table --cli-input-json file://${cancelresub_container_repo_path}/internal/clients/dynamo/createschedulestable.json
awslocal dynamodb create-table --cli-input-json file://${cancelresub_container_repo_path}/internal/clients/dynamo/createeventstable.json
echo creating tables done
