#!/usr/bin/env sh

cancelresub_container_repo_path="/tmp/subscription-cancel-and-resubscribe"

echo "creating tables ..."

## SUBSCRIPTION API
#awslocal OR aws --endpoint "http://localhost:4566"
awslocal dynamodb create-table --cli-input-json "file:///var/tmp/createidempotency.json"
awslocal dynamodb create-table --cli-input-json "file:///var/tmp/createmappingtable.json"
awslocal dynamodb create-table --cli-input-json "file:///var/tmp/idmappingtable.json"

## SUBSCRIPTION CANCEL RESUBSCRIPTION
awslocal dynamodb create-table --cli-input-json "file:///var/tmp/subscription_cancel_and_recreate_events.json"
awslocal dynamodb create-table --cli-input-json "file:///var/tmp/scheduled_updates.json"

## NOTIFICATION API
awslocal dynamodb create-table --cli-input-json "file:///var/tmp/notifications_details.json"
awslocal dynamodb create-table --cli-input-json "file:///var/tmp/retry_message_config.json"
awslocal dynamodb create-table --cli-input-json "file:///var/tmp/message_retry_schedule.json"

## EVENTS ROUTER
awslocal dynamodb create-table --cli-input-json "file:///var/tmp/retry_schechule_web_processor.json"

## PRICE CHANGE NOTIFICATIONS
awslocal dynamodb create-table --cli-input-json "file:///var/tmp/create-ddb-subscription-plan-mapping.json"
awslocal dynamodb create-table --cli-input-json "file:///var/tmp/create-ddb-subscriptions-price-changes.json"

echo "creating tables done"
