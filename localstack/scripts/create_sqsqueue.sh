#!/usr/bin/env sh

echo "creating sqs queue..."

#aws --endpoint-url=http://localhost:4566 sqs delete-queue --queue-url http://localhost:4566/000000000000/NotificationApi.fifo
aws --endpoint "http://localhost:4566" sqs create-queue --queue-name "NotificationApi.fifo" --attributes '{"FifoQueue":"true"}'
#awslocal sqs create-queue --queue-name "NotificationApi.fifo" --attributes '{"FifoQueue":"true"}'

#aws --endpoint-url=http://localhost:4566 sqs delete-queue --queue-url http://localhost:4566/000000000000/EventsApiQuque.fifo
aws --endpoint "http://localhost:4566" sqs create-queue --queue-name "EventsApiQuque.fifo" --attributes '{"FifoQueue":"true","ContentBasedDeduplication":"true"}'
#awslocal sqs create-queue --queue-name "EventsApiQuque.fifo" --attributes '{"FifoQueue":"true","ContentBasedDeduplication":"true"}'

#aws --endpoint-url=http://localhost:4566 sqs delete-queue --queue-url http://localhost:4566/000000000000/ReadEventsQueueDLQ.fifo
aws --endpoint "http://localhost:4566" sqs create-queue --queue-name "ReadEventsQueueDLQ.fifo" --attributes '{"FifoQueue":"true","ContentBasedDeduplication":"true"}'
#awslocal sqs create-queue --queue-name "ReadEventsQueueDLQ.fifo" --attributes '{"FifoQueue":"true","ContentBasedDeduplication":"true"}'

#aws --endpoint-url=http://localhost:4566 sqs delete-queue --queue-url http://localhost:4566/000000000000/ReadEventsQueue.fifo
aws --endpoint "http://localhost:4566" sqs create-queue --queue-name "ReadEventsQueue.fifo" --attributes '{"FifoQueue":"true","ContentBasedDeduplication":"true"}'
#awslocal sqs create-queue --queue-name "ReadEventsQueue.fifo" --attributes '{"FifoQueue":"true","ContentBasedDeduplication":"true"}'

#aws --endpoint-url=http://localhost:4566 sqs delete-queue --queue-url http://localhost:4566/000000000000/sqs-localstack-subscriptions-events.fifo
aws --endpoint "http://localhost:4566" sqs create-queue --queue-name "sqs-localstack-subscriptions-events.fifo" --attributes '{"FifoQueue":"true","ContentBasedDeduplication":"true"}'
#awslocal sqs create-queue --queue-name "ReadEventsQueue.fifo" --attributes '{"FifoQueue":"true","ContentBasedDeduplication":"true"}'


echo "creating sqs queue done"
