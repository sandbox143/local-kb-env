#!/bin/bash

echo "creating secrets..."

#awslocal -> aws --endpoint-url=http://localhost:4566
#aws --endpoint "http://localhost:4566" secretsmanager delete-secret --secret-id secrets_Mysql_DB_Manager --force-delete-without-recovery
aws --endpoint "http://localhost:4566" secretsmanager create-secret \
    --name secrets_Mysql_DB_Manager \
    --description "test secret for MySql DB for CONTAINER" \
    --secret-string "{\"username\": \"test\",  \"password\": \"test\",  \"host\": \"mysql_db\",  \"port\": 3308,  \"dbname\": \"events\",  \"dbClusterIdentifier\": \"scheduled-events-mysql\"}"
echo "creating secrets secrets_Mysql_DB_Manager for Mysql DB done"

#aws --endpoint "http://localhost:4566" secretsmanager delete-secret --secret-id secrets_Mysql_DB_Manager_LOCAL --force-delete-without-recovery
aws --endpoint "http://localhost:4566" secretsmanager create-secret \
    --name secrets_Mysql_DB_Manager_LOCAL \
    --description "test secret for MySql DB for LOCALHOST" \
    --secret-string "{\"username\": \"test\",  \"password\": \"test\",  \"host\": \"localhost\",  \"port\": 3308,  \"dbname\": \"events\",  \"dbClusterIdentifier\": \"scheduled-events-mysql\"}"
echo "creating secret secrets_Mysql_DB_Manager_LOCAL for Mysql DB done"

#aws --endpoint "http://localhost:4566" secretsmanager delete-secret --secret-id asm-d-ew2-local-reporting-rds-password --force-delete-without-recovery
aws --endpoint "http://localhost:4566" secretsmanager create-secret \
    --name asm-d-ew2-local-reporting-rds-password \
    --description "asm d ew2 local reporting rds password" \
    --secret-string "{\"username\": \"admin\", \"password\": \"1\", \"dbname\": \"devreporting\", \"port\": 5432, \"host\":\"localhost\"}"
echo "creating secret asm-d-ew2-local-reporting-rds-password for Reporting"

#aws --endpoint "http://localhost:4566" secretsmanager delete-secret --secret-id AmazonMSK_asm-d-ew2-d-msk-shared-cluster-credentials --force-delete-without-recovery
aws --endpoint "http://localhost:4566" secretsmanager create-secret \
    --name AmazonMSK_asm-d-ew2-d-msk-shared-cluster-credentials \
    --description "test secret for Events Listener" \
    --secret-string "{\"username\": \"client\", \"password\": \"client-secret\"}"
echo "creating secret secrets_Mysql_DB_Manager_LOCAL for Mysql DB done"



echo "creating secrets done"
