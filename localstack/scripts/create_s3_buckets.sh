#!/usr/bin/env sh

echo "creating s3 buckets..."

awslocal s3api create-bucket --bucket vz-prod
awslocal s3api create-bucket --bucket vz-tt1
awslocal s3api create-bucket --bucket vz-dev

echo "creating s3 buckets done"