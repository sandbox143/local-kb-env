#!/usr/bin/env sh

echo "creating secrets with variables..."

##
## For using subscription-api in docker container use "killbill" for debug use "localhost"
##
testEndpoint=killbill
#testEndpoint=localhost

notificationProcessorEndpoint=killbill
#notificationProcessorEndpoint=localhost

## To use Cnr flow, add in the "secret-string": \"HasCustomCancelAndRecreateFlow\":\"true\",\"HasCustomCancelAndRecreateFlowForAllMerchants\":\"true\"

#awslocal --endpoint "http://localhost:4566" secretsmanager delete-secret --secret-id test --force-delete-without-recovery
awslocal secretsmanager delete-secret --secret-id test --force-delete-without-recovery --region eu-west-2
#aws --endpoint "http://localhost:4566" secretsmanager create-secret \
awslocal secretsmanager --region eu-west-2 create-secret \
    --name test \
    --kms-key-id test \
    --description "test secret for partner" \
    --secret-string "{\"KbUsername\": \"admin\",  \"KbPassword\": \"password\",  \"KbHost\": \""${testEndpoint}":8080\",  \"KbApiKey\": \"bob\",
    \"KbApiSecret\": \"lazar\",  \"BasicPassword\": \"test\",  \"BasicUser\": \"test\",  \"HasDefaultPayment\": \"true\",  \"HasCustomCancelStateFlow\": \"false\",
    \"HasSplitBilling\": \"false\", \"HasCustomCancelAndRecreateFlow\":\"true\", \"HasCustomCancelAndRecreateFlowForAllMerchants\":\"true\", \"hasPriceChangeEnabled\":\"true\",
    \"kbCacheHostResolver\": \""${testEndpoint}":8080\"}"
echo "creating secrets for test done"

## Configuration for another tenant bob1"
#aws --endpoint "http://localhost:4566" secretsmanager delete-secret --secret-id test1 --force-delete-without-recovery
awslocal secretsmanager delete-secret --secret-id test1 --force-delete-without-recovery --region eu-west-2
#aws --endpoint "http://localhost:4566" secretsmanager create-secret \
awslocal secretsmanager --region eu-west-2 create-secret \
    --name test1 \
    --kms-key-id test1 \
    --description "test1 secret for partner" \
    --secret-string "{\"KbUsername\": \"admin\",  \"KbPassword\": \"password\",  \"KbHost\": \""${testEndpoint}":8080\",  \"KbApiKey\": \"bob1\",
    \"KbApiSecret\": \"lazar1\",  \"BasicPassword\": \"test1\",  \"BasicUser\": \"test1\",  \"HasDefaultPayment\": \"true\",  \"HasCustomCancelStateFlow\": \"false\",
    \"HasSplitBilling\": \"false\", \"HasCustomCancelAndRecreateFlow\":\"true\", \"HasCustomCancelAndRecreateFlowForAllMerchants\":\"true\", \"hasPriceChangeEnabled\":\"true\,
    \"kbCacheHostResolver\": \""${testEndpoint}":8080\"}"
echo "creating secrets for test1 done"

#aws --endpoint "http://localhost:4566" secretsmanager delete-secret --secret-id db1 --force-delete-without-recovery
awslocal secretsmanager delete-secret --secret-id db1 --force-delete-without-recovery --region eu-west-2
#aws --endpoint "http://localhost:4566" secretsmanager create-secret \
awslocal secretsmanager --region eu-west-2 create-secret \
    --name db1 \
    --description "For Notification Processor" \
    --secret-string "{\"tlsEnabled\": \"false\", \"Host\": \"${notificationProcessorEndpoint}\", \"Port\": \"3306\", \"DbName\": \"killbill\", \"Username\": \"root\", \"Password\": \"killbill\",\"engine\":\"mysql\"}"
echo "creating secret db1 for KB Mysql DB done"

##
## For using subscription-cancel-and-resubscribe in docker container use "killbill" for debug use "localhost"
##
testEndpoint=killbill
#testEndpoint=localhost

#aws --endpoint "http://localhost:4566" secretsmanager delete-secret --secret-id CnRtest --force-delete-without-recovery
awslocal secretsmanager delete-secret --secret-id CnRtest --force-delete-without-recovery --region eu-west-2
#aws --endpoint "http://localhost:4566" secretsmanager create-secret \
awslocal secretsmanager --region eu-west-2 create-secret \
    --name CnRtest \
    --description "test secret for partner" \
    --secret-string "{\"KbUsername\": \"admin\",  \"KbPassword\": \"password\",  \"KbHost\": \""${testEndpoint}":8080\",  \"KbApiKey\": \"bob\",  \"KbApiSecret\": \"lazar\",  \"BasicPassword\": \"test\",  \"BasicUser\": \"test\",  \"HasDefaultPayment\": \"true\",  \"HasCustomCancelStateFlow\": \"false\",  \"HasSplitBilling\": \"false\", \"hasCustomCancelAndRecreateFlow\": \"true\"}"
echo "creating secrets for cancel and resubscribe done"

echo "creating secrets with variables done"