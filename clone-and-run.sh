#!/bin/sh
set -e
bold=$(tput bold)
normal=$(tput sgr0)

cloneOrPullGitRepo()
{
	url=$1
	dirname=$2
	if cd ${dirname} 2>/dev/null
	then
		echo pulling ${url}
		git pull
		cd ..
	else
		git clone --progress -v ${url} ${dirname}
	fi
}

echo ${bold}cloning Bango repositories...${normal}
cloneOrPullGitRepo "https://gitlab.com/bango/cloud-payments-platform/subscription-engine/killbill-payment-plugin.git" killbill-payment-plugin
cloneOrPullGitRepo "https://gitlab.com/bango/cloud-payments-platform/subscription-engine/killbill-catalog-plugin.git" killbill-catalog-plugin
cloneOrPullGitRepo "https://gitlab.com/bango/cloud-payments-platform/subscription-engine/killbill-notification-plugin.git" killbill-notification-plugin
cloneOrPullGitRepo "https://gitlab.com/bango/cloud-payments-platform/subscription-engine/bt-adapter.git" bt-adapter
cloneOrPullGitRepo "https://gitlab.com/bango/cloud-payments-platform/subscription-engine/payment-api.git" payment-api
cloneOrPullGitRepo "https://gitlab.com/bango/cloud-payments-platform/subscription-engine/subscription-api.git" subscription-api
cloneOrPullGitRepo "https://gitlab.com/bango/cloud-payments-platform/subscription-engine/notification-api.git" notification-api
cloneOrPullGitRepo "https://gitlab.com/bango/cloud-payments-platform/subscription-engine/subscription-cancel-and-resubscribe.git" subscription-cancel-and-resubscribe
cloneOrPullGitRepo "https://gitlab.com/sandbox143/identity-mock.git" soap-mock
cloneOrPullGitRepo "https://gitlab.com/bango/core/events-api.git" events-api
cloneOrPullGitRepo "https://gitlab.com/bango/core/events-router.git" events-router
cloneOrPullGitRepo "https://gitlab.com/bango/core/standard-web-request-processor.git" web-processor
cloneOrPullGitRepo "https://gitlab.com/bango/subscriptions/notifications-orchestrator.git" notifications-orchestrator
cloneOrPullGitRepo "https://gitlab.com/bango/subscriptions/notifications-processor.git" notifications-processor
cloneOrPullGitRepo "https://gitlab.com/bango/cloud-payments-platform/subscription-engine/killbill-catalog-updater.git" killbill-catalog-updater
#cloneOrPullGitRepo "https://gitlab.com/bango/cloud-payments-platform/subscription-engine/killbill-subscription-events-listener.git" killbill-subscription-events-listener

echo ${bold}stop and remove docker sh...${normal}
./stop-and-remove.sh

echo ${bold}starting docker compose...${normal}
docker-compose up -d

echo ${bold}initial kb provisioning...${normal}
kill_bill/provision.sh

echo ${bold}restart "subscription-cancel-and-resubscribe" if not ready...${normal}
while [[ "$(curl -s -o /dev/null -w ''%{http_code}'' http://localhost:8084/k8s/probes/ready)" != "200" ]];
do
   echo "subscription-cancel-and-resubscribe not ready, restarting..."
   docker-compose restart subscription-cancel-and-resubscribe
   sleep 3
done
echo ${bold}"subscription-cancel-and-resubscribe is ready"${normal}

echo ${bold}kb local env done${normal}
